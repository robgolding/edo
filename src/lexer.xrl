Definitions.

Whitespace = \s
Indent = \t
Newline = (\n|\r\n|\r)+

Letter = [a-z|A-Z]
Symbol = .|-|_
Number = [0-9]
Character = {Letter}|{Number}|{Symbol}
Anything = (.)

Word = ([a-zA-Z._-])+

Rules.

{Whitespace} : skip_token.
{Newline} : {token, {newline, TokenLine, TokenChars}}.
\: : {token, {':', TokenLine, TokenChars}}.
{Indent} : {token, {indent, TokenLine, TokenChars}}.
{Word} : {token, {word, TokenLine, TokenChars}}.

Erlang code.
