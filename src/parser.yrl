Terminals
  ':'
  indent
  newline
  word
.

Nonterminals
  targets
  target
  script
  words
.

Rootsymbol targets.

targets -> target : ['$1'].
targets -> target targets : ['$1' | '$2'].

target -> word ':' words newline script : {unwrap('$1'), '$3', '$5'}.
target -> word ':' words newline : {'$1', '$3', []}.

script -> indent words newline : ['$2'].
script -> indent words newline script : ['$2' | '$4'].

words -> word : [unwrap('$1')].
words -> word words : [unwrap('$1')] ++ '$2'.

Erlang code.

unwrap({_, _, Value}) -> Value.
