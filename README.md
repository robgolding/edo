# Edo

Edo is a `make`-like task runner, written in Elixir.

## Building

```
$ mix escript.build
```
