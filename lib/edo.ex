defmodule Edo do
  @moduledoc """
  Documentation for Edo.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Edo.hello()
      :world

  """
  def hello do
    :world
  end
end
