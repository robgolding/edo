defmodule Edo.CLI do
  alias Edo.Target

  def main(argv) do
    {target_names, options} = parse_args(argv)
    filename = Keyword.get(options, :filename, "Edofile")
    text = File.read!(filename)
    {:ok, tokens, _} = :lexer.string(to_charlist(text))
    {:ok, tree} = :parser.parse(tokens)

    if Keyword.get(options, :verbose, false) do
      IO.inspect(tree)
    end

    targets = create_targets(tree)

    if Keyword.get(options, :verbose, false) do
      IO.inspect(targets)
    end

    Enum.each(target_names, &process(targets, &1))
  end

  def parse_args(argv) do
    {options, args, _} =
      OptionParser.parse(argv,
        strict: [
          filename: :string,
          verbose: :boolean
        ],
        aliases: [
          f: :filename,
          v: :verbose
        ]
      )

    {args, options}
  end

  def create_targets(tree) do
    Enum.map(tree, &create_target/1)
  end

  def create_target(target_tree) do
    {t_name, t_deps, t_script} = target_tree

    %Target{
      name: to_string(t_name),
      deps: Enum.map(t_deps, &to_string/1),
      script:
        Enum.map(t_script, fn t_script_line ->
          Enum.join(Enum.map(t_script_line, &to_string/1), " ")
        end)
    }
  end

  def process(targets, target_name \\ nil) do
    target_name = target_name || targets[0].name

    case resolve(targets, target_name) do
      false -> IO.puts("Nothing to do for " <> target_name)
      true -> nil
    end
  end

  def target_by_name(targets, name) do
    Enum.find(targets, fn target -> target.name == name end)
  end

  def run_cmd(cmd) do
    IO.puts(cmd)
    :os.cmd(to_charlist(cmd))
  end

  def run_script(script) do
    Enum.each(script, &run_cmd/1)
  end

  def cmp(name, dep) do
    case File.stat(name) do
      {:ok, f_stat} ->
        {:ok, d_stat} = File.stat(dep)

        cond do
          d_stat.mtime > f_stat.mtime ->
            true

          true ->
            false
        end

      {:error, _reason} ->
        true
    end
  end

  def is_stale(target) do
    if File.exists?(target.name) do
      Enum.any?(Enum.map(target.deps, &cmp(target.name, &1)))
    else
      true
    end
  end

  def resolve(targets, name) do
    case target_by_name(targets, name) do
      nil ->
        false

      target ->
        Enum.each(target.deps, &resolve(targets, &1))

        case is_stale(target) do
          true ->
            run_script(target.script)
            true

          false ->
            false
        end
    end
  end
end
