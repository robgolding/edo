defmodule Edo.Target do
  defstruct name: nil, deps: [], script: []
end
