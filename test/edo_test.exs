defmodule EdoTest do
  use ExUnit.Case
  alias Edo.CLI
  alias Edo.Target
  doctest Edo

  @tree_single [
    {
      'a.out',
      ['main.c', 'main.o'],
      [
        ['gcc', '-c', 'main.c'],
        ['gcc', 'main.o']
      ]
    }
  ]

  @targets_single [
    %Target{
      name: "a.out",
      deps: ["main.c", "main.o"],
      script: ["gcc -c main.c", "gcc main.o"]
    }
  ]

  @tree_basic [
    {
      'a.out',
      ['main.o'],
      [
        ['gcc', 'main.o']
      ]
    },
    {
      'main.o',
      ['main.c'],
      [
        ['gcc', '-c', 'main.c']
      ]
    }
  ]

  @targets_basic [
    %Target{
      name: "a.out",
      deps: ["main.o"],
      script: ["gcc main.o"]
    },
    %Target{
      name: "main.o",
      deps: ["main.c"],
      script: ["gcc -c main.c"]
    }
  ]

  test "parses Edofile.single" do
    {:ok, content} = File.read("test/fixtures/Edofile.single")
    {:ok, tokens, _} = :lexer.string(to_charlist(content))
    {:ok, tree} = :parser.parse(tokens)
    assert tree == @tree_single
  end

  test "creates single target" do
    assert CLI.create_targets(@tree_single) == @targets_single
  end

  test "parses Edofile.basic" do
    {:ok, content} = File.read("test/fixtures/Edofile.basic")
    {:ok, tokens, _} = :lexer.string(to_charlist(content))
    {:ok, tree} = :parser.parse(tokens)
    assert tree == @tree_basic
  end

  test "creates basic targets" do
    assert CLI.create_targets(@tree_basic) == @targets_basic
  end

  test "target_by_name" do
    assert CLI.target_by_name(@targets_single, "a.out").name == "a.out"
    assert CLI.target_by_name(@targets_basic, "main.o").name == "main.o"
  end
end
