defmodule Edo.MixProject do
  use Mix.Project

  def project do
    [
      app: :edo,
      version: "0.1.0",
      elixir: "~> 1.7",
      escript: escript(),
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  def escript do
    [main_module: Edo.CLI]
  end

  defp deps do
    []
  end
end
